module.exports = {
    mongoURI: '',
    cookieKey: '',
    sessionKey: '',
    tokenSecret: '',
    googleRecaptchaSecret: '',
    googleClientId: '',
    googleClientSecret: '',
    facebookClientId: '',
    facebookClientSecret: '',
    twitterConsumerId: '',
    twitterConsumerSecret: '',
    redirectDomain: 'http://localhost:3000'
};
